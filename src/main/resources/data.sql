INSERT INTO roles (name)
VALUES ('ADMIN');
INSERT INTO roles (name)
VALUES ('USER');


INSERT INTO persons (name, password, date_of_birth, ipn)
VALUES ('Mark', '$2a$12$HaU7lLiJJ0u1CnVWQDAJgeHZh18tvTf4pQ33TMavUutE/RnghGoD.', '1982-09-23', '3021623415');
INSERT INTO persons (name, password, date_of_birth, ipn)
VALUES ('John', '$2a$12$/zarW8xt2vJjr8fA4F2uOOPkwfLI1eaFjkLeAwETdI9zJ2m2FKTFu', '1989-10-14', '3279423759');
INSERT INTO persons (name, password, date_of_birth, ipn)
VALUES ('Kate', '$2a$12$JfmciiifksFwk7kj2SB6vOwD6rKguF/Xj5CKLj2WJ2WaeuvXs.D1e', '2002-01-05', '3726039414');
INSERT INTO persons (name, password, date_of_birth, ipn)
VALUES ('Bard', '$2a$12$AQ.h2jqth9f5wjepVOEWguqh2hxlEE9.5gicxUCzJPW4ON3dD.ez.', '2005-04-21', '3726012344');


-- INSERT INTO persons (name, password, date_of_birth, ipn, role_id) VALUES ('Mark', '123', '1982-09-23', '3021623415', 1);
-- INSERT INTO persons (name, password, date_of_birth, ipn, role_id) VALUES ('John', '1', '1989-10-14', '3279423759', 2);
-- INSERT INTO persons (name, password, date_of_birth, ipn, role_id) VALUES ('Kate', '2', '2002-01-05', '3726039414', 2);
-- INSERT INTO persons (name, password, date_of_birth, ipn, role_id) VALUES ('Bard', '3', '2005-04-21', '3726012344', 2);
-- INSERT INTO persons (name, password, date_of_birth, ipn, role_id) VALUES ('Denzel', '123', '1982-06-01', '3645903217', 2);
-- INSERT INTO persons (name, password, date_of_birth, ipn, role_id) VALUES ('Li', '123', '1990-11-23', '3234573984', 2);
-- INSERT INTO persons (name, password, date_of_birth, ipn, role_id) VALUES ('Bred', '123', '2010-04-15', '3647589024', 2);
-- INSERT INTO persons (name, password, date_of_birth, ipn, role_id) VALUES ('Tonny', '123', '1982-03-11', '3458950342', 2);
-- INSERT INTO persons (name, password, date_of_birth, ipn, role_id) VALUES ('James', '123', '1971-07-30', '3657861204', 2);
-- INSERT INTO persons (name, password, date_of_birth, ipn, role_id) VALUES ('Ashly', '123', '1997-12-24', '3475901202', 2);
-- INSERT INTO persons (name, password, date_of_birth, ipn, role_id) VALUES ('Michail', '123', '1992-04-19', '3371204458', 2);
-- INSERT INTO persons (name, password, date_of_birth, ipn, role_id) VALUES ('Darina', '123', '2003-03-15', '3769407665', 2);
-- INSERT INTO persons (name, password, date_of_birth, ipn, role_id) VALUES ('Olga', '123', '1970-12-10', '2591106321', 2);


INSERT INTO tasks (name, planned_time, status, person_id)
VALUES ('wake up', '2023-10-11 08:00', 'PLANNED', 2);
INSERT INTO tasks (name, planned_time, status, person_id)
VALUES ('exercise', '2023-10-11 08:20', 'PLANNED', 2);
INSERT INTO tasks (name, planned_time, status, person_id)
VALUES ('breakfast', '2023-10-11 09:50', 'PLANNED', 2);
INSERT INTO tasks (name, planned_time, status, person_id)
VALUES ('work', '2023-10-11 10:30', 'PLANNED', 2);

INSERT INTO tasks (name, planned_time, status, person_id)
VALUES ('wake up', '2023-10-11 08:00', 'PLANNED', 4);
INSERT INTO tasks (name, planned_time, status, person_id)
VALUES ('exercise', '2023-10-11 09:00', 'PLANNED', 4);
INSERT INTO tasks (name, planned_time, status, person_id)
VALUES ('breakfast', '2023-10-11 10:00', 'PLANNED', 4);


INSERT INTO persons_roles (person_id, role_id)
VALUES (1, 1);
INSERT INTO persons_roles (person_id, role_id)
VALUES (1, 2);
INSERT INTO persons_roles (person_id, role_id)
VALUES (2, 2);
INSERT INTO persons_roles (person_id, role_id)
VALUES (3, 2);
INSERT INTO persons_roles (person_id, role_id)
VALUES (4, 2);