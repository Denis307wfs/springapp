package com.stepanov.spring.config;

import com.stepanov.spring.repository.PersonRepository;
import com.stepanov.spring.service.PersonDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;

@EnableGlobalMethodSecurity(prePostEnabled = true)
@EnableWebSecurity
@Configuration
public class SecurityConfig {
    private final PersonRepository personRepository;
    private final PersonDetailsService personDetailsService;

    @Autowired
    public SecurityConfig(PersonRepository personRepository, PersonDetailsService personDetailsService) {
        this.personRepository = personRepository;
        this.personDetailsService = personDetailsService;
    }

    @Bean
    public UserDetailsService userDetailsService() {
        return new PersonDetailsService(personRepository);
    }

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        http
                .csrf().disable()
                .cors().disable()
                .authorizeHttpRequests(authorizeHttpRequests ->
                        authorizeHttpRequests
                                .antMatchers("/actuator/**",
                                        "/swagger-ui/**",
                                        "/v3/api-docs")
                                .permitAll()
                                .anyRequest()
                                .authenticated())
                .userDetailsService(personDetailsService)
                .formLogin(Customizer.withDefaults())
                .logout()
                .logoutSuccessUrl("/login#/")
                .and()
                .httpBasic();
        return http.build();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}

