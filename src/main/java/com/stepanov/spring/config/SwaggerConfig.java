package com.stepanov.spring.config;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.security.SecurityRequirement;
import io.swagger.v3.oas.models.security.SecurityScheme;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
@OpenAPIDefinition
public class SwaggerConfig {
    private static final String SECURITY_SCHEMA_NAME = "Basic authorize";
    @Value("${app.version}")
    private String appVersion;
    @Value("${app.description}")
    private String description;

    @Bean
    public OpenAPI baseOpenAPI() {
        return new OpenAPI()
                .addSecurityItem(new SecurityRequirement()
                        .addList(SECURITY_SCHEMA_NAME))
                .components(new Components()
                        .addSecuritySchemes(SECURITY_SCHEMA_NAME, createAPIKeyScheme()))
                .info(new Info()
                        .title("Tasks web service")
                        .version(appVersion));
    }

    @Bean
    public SecurityScheme createAPIKeyScheme() {
        return new SecurityScheme()
                .name(SECURITY_SCHEMA_NAME)
                .type(SecurityScheme.Type.HTTP)
                .scheme("basic");
    }
}
