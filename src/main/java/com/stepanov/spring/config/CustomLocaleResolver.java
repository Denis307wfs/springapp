package com.stepanov.spring.config;

import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.LocaleResolver;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

@Component
public class CustomLocaleResolver implements LocaleResolver {
    private static final Locale DEFAULT_LOCALE = Locale.US;

    private final List<Locale> localeList = Arrays.asList(
            new Locale("en"),
            new Locale("uk")
    );

    @Override
    @NonNull
    public Locale resolveLocale(HttpServletRequest request) {
        String headerLang = request.getHeader("Accept-Language");
        Locale locale;
        try {
            locale = Locale.lookup(Locale.LanguageRange.parse(headerLang), localeList);
            return locale == null ? DEFAULT_LOCALE : locale;
        } catch (IllegalArgumentException | NullPointerException ex) {
            return DEFAULT_LOCALE;
        }
    }

    @Override
    public void setLocale(@NonNull HttpServletRequest request, HttpServletResponse response, Locale locale) {
        LocaleContextHolder.setLocale(this.resolveLocale(request));
    }
}
