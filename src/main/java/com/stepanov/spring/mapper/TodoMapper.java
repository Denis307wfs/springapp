package com.stepanov.spring.mapper;

import com.stepanov.spring.config.MapperConfig;
import com.stepanov.spring.dto.CreateTodoDTO;
import com.stepanov.spring.dto.TodoDTO;
import com.stepanov.spring.entity.Todo;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

@Mapper(config = MapperConfig.class)
public interface TodoMapper {
    @Mapping(target = "plannedTime", dateFormat = "yyyy-MM-dd HH:mm")
    Todo update(@MappingTarget Todo todo, TodoDTO todoDTO);
    @Mapping(target = "plannedTime", dateFormat = "yyyy-MM-dd HH:mm")
    Todo create(CreateTodoDTO todoDTO);

    @Mapping(target = "plannedTime", dateFormat = "yyyy-MM-dd HH:mm")
    TodoDTO todoToTodoDTO(Todo todo);
    @Mapping(target = "plannedTime", dateFormat = "yyyy-MM-dd HH:mm")
    Todo todoDTOtoTodo(TodoDTO todoDTO);

    @Mapping(target = "plannedTime", dateFormat = "yyyy-MM-dd HH:mm")
    Todo createTodoDTOtoTodo(CreateTodoDTO todoDTO);
}
