package com.stepanov.spring.mapper;

import com.stepanov.spring.config.MapperConfig;
import com.stepanov.spring.dto.PersonDTO;
import com.stepanov.spring.entity.Person;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

@Mapper(config = MapperConfig.class)
public interface PersonMapper {
    Person update(@MappingTarget Person person, PersonDTO personDTO);
    PersonDTO personToPersonDTO(Person person);
    Person personDtoToPerson(PersonDTO personDTO);
}
