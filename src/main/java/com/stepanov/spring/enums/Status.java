package com.stepanov.spring.enums;

import java.util.Set;

public enum Status {
    PLANNED {
        @Override
        public Set<Status> nextStatus() {
            return Set.of(WORK_IN_PROGRESS, POSTPONED, CANCELLED);
        }
    },
    WORK_IN_PROGRESS {
        @Override
        public Set<Status> nextStatus() {
            return Set.of(NOTIFIED, SIGNED, CANCELLED);
        }
    },
    POSTPONED {
        @Override
        public Set<Status> nextStatus() {
            return Set.of(NOTIFIED, SIGNED, CANCELLED);
        }
    },
    NOTIFIED {
        @Override
        public Set<Status> nextStatus() {
            return Set.of(DONE, CANCELLED);
        }
    },
    SIGNED {
        @Override
        public Set<Status> nextStatus() {
            return Set.of(DONE, CANCELLED);
        }
    },
    DONE {
        @Override
        public Set<Status> nextStatus() {
            return Set.of(DONE);
        }
    },
    CANCELLED {
        @Override
        public Set<Status> nextStatus() {
            return Set.of(CANCELLED);
        }
    };

    public abstract Set<Status> nextStatus();
}