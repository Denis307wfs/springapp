package com.stepanov.spring.controller;

import com.stepanov.spring.dto.NoIdPersonDTO;
import com.stepanov.spring.dto.PersonDTO;
import com.stepanov.spring.service.PersonService;
import com.stepanov.spring.util.LogExecutionTime;
import com.stepanov.spring.util.ResponseUtil;
import com.stepanov.spring.util.Translator;
import org.springdoc.api.annotations.ParameterObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;
import java.util.Map;


@RestController
@PreAuthorize("hasAnyAuthority('ADMIN')")
@RequestMapping("/persons")
public class PersonController {
    private final PersonService personService;

    @Autowired
    public PersonController(PersonService personService) {
        this.personService = personService;
    }


    @LogExecutionTime
    @PostMapping
    public ResponseEntity<PersonDTO> create(@Valid @RequestBody NoIdPersonDTO requestPerson, UriComponentsBuilder ucb) {
        PersonDTO createdPerson = personService.save(requestPerson);

        URI locationOfNewPerson = ucb
                .path("/persons/{id}")
                .buildAndExpand(createdPerson.id())
                .toUri();

        return ResponseEntity.created(locationOfNewPerson).body(createdPerson);
    }

    @LogExecutionTime
    @GetMapping("/")
    public ResponseEntity<List<PersonDTO>> getAll(@ParameterObject Pageable pageable) {
        return ResponseEntity.ok(personService.getAll(pageable));
    }

    @LogExecutionTime
    @GetMapping("/{id}")
    public ResponseEntity<PersonDTO> findById(@PathVariable Long id) {
        return ResponseEntity.ok(personService.getById(id));
    }

    @LogExecutionTime
    @PutMapping("/{id}")
    public ResponseEntity<PersonDTO> update(@PathVariable Long id, @Valid @RequestBody NoIdPersonDTO requestPerson) {
        return ResponseEntity.ok(personService.update(id, requestPerson));
    }

    @LogExecutionTime
    @DeleteMapping("/{id}")
    public ResponseEntity<Object> deleteById(@PathVariable Long id) {
        personService.deleteById(id);
        Map<String, Object> body = ResponseUtil.getObjectMap(
                String.format(Translator.toLocale("person.id.deleted"), id));
        return ResponseEntity.ok(body);
    }
}
