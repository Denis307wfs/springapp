package com.stepanov.spring.controller;

import com.stepanov.spring.dto.AdminChangeTodoDTO;
import com.stepanov.spring.dto.CreateTodoDTO;
import com.stepanov.spring.dto.TodoDTO;
import com.stepanov.spring.service.TodoService;
import com.stepanov.spring.util.LogExecutionTime;
import com.stepanov.spring.util.ResponseUtil;
import com.stepanov.spring.util.Translator;
import io.micrometer.core.annotation.Timed;
import org.springdoc.api.annotations.ParameterObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

@PreAuthorize("hasAnyAuthority('ADMIN')")
@RestController
@RequestMapping("/tasks")
public class TodoController {
    private final TodoService todoService;

    @Autowired
    public TodoController(TodoService todoService) {
        this.todoService = todoService;
    }

    @Timed("createTask")
    @LogExecutionTime
    @PreAuthorize("hasAnyAuthority('USER')")
    @PostMapping
    public ResponseEntity<TodoDTO> create(@Valid @RequestBody CreateTodoDTO todoDTO) {
        TodoDTO saved = todoService.save(todoDTO);
        return ResponseEntity.status(HttpStatus.CREATED).body(saved);
    }

    @Timed("getAllTask")
    @LogExecutionTime
    @GetMapping
    public ResponseEntity<List<TodoDTO>> getAll(@ParameterObject Pageable pageable) {
        return ResponseEntity.ok(todoService.getAll(pageable));
    }

    @Timed("getUserTasks")
    @LogExecutionTime
    @PreAuthorize("hasAnyAuthority('USER')")
    @GetMapping("/me")
    public ResponseEntity<List<TodoDTO>> getTasks(@ParameterObject Pageable pageable) {
        return ResponseEntity.ok(todoService.getAllForPerson(pageable));
    }

    @Timed("getTaskById")
    @LogExecutionTime
    @PreAuthorize("hasAnyAuthority('USER')")
    @GetMapping("/{id}")
    public ResponseEntity<TodoDTO> getById(@PathVariable Long id) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return ResponseEntity.ok(todoService.getById(authentication, id));
    }

    @Timed("changeTaskById")
    @LogExecutionTime
    @PutMapping("/{id}")
    public ResponseEntity<TodoDTO> update(@PathVariable Long id, @Valid @RequestBody AdminChangeTodoDTO requestTask) {
        return ResponseEntity.ok(todoService.update(id, requestTask));
    }

    @Timed("deleteTaskById")
    @LogExecutionTime
    @DeleteMapping("/{id}")
    public ResponseEntity<Object> deleteById(@PathVariable Long id) {
        todoService.deleteById(id);
        Map<String, Object> body = ResponseUtil.getObjectMap(
                String.format(Translator.toLocale("task.id.deleted"), id));
        return ResponseEntity.ok(body);
    }
}
