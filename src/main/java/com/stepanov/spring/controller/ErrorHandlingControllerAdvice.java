package com.stepanov.spring.controller;

import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import com.stepanov.spring.exception.*;
import com.stepanov.spring.util.Translator;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.time.format.DateTimeParseException;
import java.util.HashMap;
import java.util.Map;

@RestControllerAdvice
public class ErrorHandlingControllerAdvice {
    private static final String MESSAGE = "message";
    private static final String STATUS = "status";
    private static final String ERROR = "error";

    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, Object> handleValidationErrors(MethodArgumentNotValidException ex) {
        String error = ex.getBindingResult().getFieldErrors()
                .stream().map(DefaultMessageSourceResolvable::getDefaultMessage).findFirst().orElseThrow();
        return getResponseBody(error, HttpStatus.BAD_REQUEST);
    }

    @ResponseBody
    @ExceptionHandler({
            PersonIsNotValidException.class,
            DateTimeException.class,
            LocalDateTimeException.class,
            StatusException.class,
            BadRequestException.class
    })
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Map<String, Object> handleBadRequestException(Exception ex) {
        return getResponseBody(ex.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ResponseBody
    @ExceptionHandler({
            PageNotFoundException.class,
            PersonNotFoundException.class,
            TaskNotFoundException.class
    })
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public Map<String, Object> handleNotFoundException(Exception ex) {
        return getResponseBody(ex.getMessage(), HttpStatus.NOT_FOUND);
    }

    @ResponseBody
    @ExceptionHandler({DateTimeParseException.class,
            HttpMessageNotReadableException.class
    })
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Map<String, Object> handleDateTimeException(Exception ex) {
        String message;
        if (ex instanceof DateTimeParseException) {
            message = "local.date.time.exception";
        } else if (ex.getCause() instanceof InvalidFormatException) {
            message = "date.time.exception";
        } else {
            message = ex.getMessage();
        }
        return getResponseBody(message, HttpStatus.BAD_REQUEST);
    }


    @ResponseBody
    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Map<String, Object> handleException(Exception ex) {
        String message;
        if (ex.getCause() != null) {
            message = ex.getCause().getMessage();
        } else {
            message = ex.getMessage();
        }
        return getResponseBody(message, HttpStatus.BAD_REQUEST);
    }

    private Map<String, Object> getResponseBody(String errorMessage, HttpStatus status) {
        Map<String, Object> responseBody = new HashMap<>();
        responseBody.put(STATUS, status.value());
        responseBody.put(ERROR, status.getReasonPhrase());
        responseBody.put(MESSAGE, Translator.toLocale(errorMessage));
        return responseBody;
    }
}
