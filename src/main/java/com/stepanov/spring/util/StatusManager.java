package com.stepanov.spring.util;

import com.stepanov.spring.dto.TodoDTO;
import com.stepanov.spring.enums.Status;
import com.stepanov.spring.exception.StatusException;
import org.springframework.stereotype.Component;

@Component
public class StatusManager {

    private StatusManager() {
    }

    public static boolean changeTaskStatus(TodoDTO todoDTO, Status status) {
        if (todoDTO.getStatus().equals(status)) return true;

        if (todoDTO.getStatus().nextStatus().contains(status)) {
            todoDTO.setStatus(status);
            return true;
        } else {
            throw new StatusException(String.format(
                    Translator.toLocale("change.status.exception"), todoDTO.getStatus(), status));
        }
    }
}
