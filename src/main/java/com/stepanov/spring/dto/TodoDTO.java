package com.stepanov.spring.dto;

import com.stepanov.spring.enums.Status;

import java.util.Objects;

public final class TodoDTO {
    private Long id;
    private String name;
    private String plannedTime;
    private Status status;

    public TodoDTO() {
    }

    public TodoDTO(
            Long id,
            String name,
            String plannedTime,
            Status status) {
        this.id = id;
        this.name = name;
        this.plannedTime = plannedTime;
        this.status = status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPlannedTime() {
        return plannedTime;
    }

    public void setPlannedTime(String plannedTime) {
        this.plannedTime = plannedTime;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (obj == null || obj.getClass() != this.getClass()) return false;
        var that = (TodoDTO) obj;
        return Objects.equals(this.id, that.id) &&
                Objects.equals(this.name, that.name) &&
                Objects.equals(this.plannedTime, that.plannedTime) &&
                Objects.equals(this.status, that.status);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, plannedTime, status);
    }

    @Override
    public String toString() {
        return "TodoDTO[" +
                "id=" + id + ", " +
                "name=" + name + ", " +
                "plannedTime=" + plannedTime + ", " +
                "status=" + status + ']';
    }

}
