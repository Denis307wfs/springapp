package com.stepanov.spring.dto;

import com.stepanov.spring.validator.ValidIpn;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import java.time.LocalDate;

public record NoIdPersonDTO(
        @NotNull
        @NotEmpty(message = "field.not.empty")
        String name,
        @NotNull
        @NotEmpty(message = "field.not.empty")
        String password,
        @Past(message = "date.in.past")
        @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
        LocalDate dateOfBirth,
        @NotEmpty
        @Length(min = 10, max = 10, message = "ipn.length")
        @ValidIpn
        String ipn) {
}
