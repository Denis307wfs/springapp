package com.stepanov.spring.dto;

import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class AdminChangeTodoDTO {
    @NotNull
    @NotEmpty(message = "field.not.empty")
    private String name;
    @NotNull
    @NotEmpty(message = "field.not.empty")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
    private String plannedTime;
    private String status;

    public AdminChangeTodoDTO() {
    }

    public AdminChangeTodoDTO(String name, String plannedTime, String status) {
        this.name = name;
        this.plannedTime = plannedTime;
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPlannedTime() {
        return plannedTime;
    }

    public void setPlannedTime(String plannedTime) {
        this.plannedTime = plannedTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
