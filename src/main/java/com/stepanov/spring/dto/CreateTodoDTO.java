package com.stepanov.spring.dto;

import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class CreateTodoDTO {
    @NotNull
    @NotEmpty(message = "field.not.empty")
    private String name;
    @NotNull
    @NotEmpty(message = "field.not.empty")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
    private String plannedTime;

    public CreateTodoDTO() {
    }

    public CreateTodoDTO(String name, String plannedTime) {
        this.name = name;
        this.plannedTime = plannedTime;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPlannedTime() {
        return plannedTime;
    }

    public void setPlannedTime(String plannedTime) {
        this.plannedTime = plannedTime;
    }
}
