package com.stepanov.spring.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.time.LocalDate;

public record PersonDTO(
        Long id,
        String name,
        @JsonIgnore
        String password,
        @JsonFormat(pattern="yyyy-MM-dd")
        LocalDate dateOfBirth,
        String ipn) {
}
