package com.stepanov.spring.service;

import com.stepanov.spring.dto.AdminChangeTodoDTO;
import com.stepanov.spring.dto.CreateTodoDTO;
import com.stepanov.spring.dto.TodoDTO;
import com.stepanov.spring.entity.Person;
import com.stepanov.spring.entity.PersonDetails;
import com.stepanov.spring.entity.Todo;
import com.stepanov.spring.enums.Status;
import com.stepanov.spring.exception.BadRequestException;
import com.stepanov.spring.exception.LocalDateTimeException;
import com.stepanov.spring.exception.TaskNotFoundException;
import com.stepanov.spring.mapper.TodoMapper;
import com.stepanov.spring.repository.TodoRepository;
import com.stepanov.spring.util.StatusManager;
import com.stepanov.spring.util.Translator;
import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.time.format.DateTimeParseException;
import java.util.List;

@Service
public class TodoServiceImpl implements TodoService {
    private final TodoRepository todoRepository;
    private final TodoMapper mapper;

    @Autowired
    public TodoServiceImpl(TodoRepository todoRepository) {
        this.todoRepository = todoRepository;
        this.mapper = Mappers.getMapper(TodoMapper.class);
    }

    @Override
    public TodoDTO save(CreateTodoDTO todoDTO) throws LocalDateTimeException {
        PersonDetails personDetails = getPersonDetailsFromContext();
        Person person = personDetails.person();
        Todo newTodo = mapper.create(todoDTO);
        newTodo.setPerson(person);
        try {
            todoRepository.save(newTodo);
        } catch (DateTimeParseException ex) {
            throw new LocalDateTimeException(Translator.toLocale("local.date.time.exception"));
        }
        return mapper.todoToTodoDTO(newTodo);
    }

    @Override
    public List<TodoDTO> getAll(Pageable pageable) {
        return todoRepository.findAll(pageable).stream().map(mapper::todoToTodoDTO).toList();
    }

    @Override
    public List<TodoDTO> getAllForPerson(Pageable pageable) {
        PersonDetails personDetails = getPersonDetailsFromContext();
        String username = personDetails.getUsername();
        return todoRepository.findAllByPersonName(pageable, username).stream().map(mapper::todoToTodoDTO).toList();
    }

    @Override
    public TodoDTO update(Long id, AdminChangeTodoDTO requestTask) throws LocalDateTimeException {
        Todo todo = getTodoById(id);
        Status statusToChange;
        try {
            statusToChange = Status.valueOf(requestTask.getStatus());
        } catch (Exception ex) {
            throw new BadRequestException(
                    String.format(Translator.toLocale("bad.request"), requestTask.getStatus()));
        }
        TodoDTO updatedTodoDTO = new TodoDTO(
                id, requestTask.getName(), requestTask.getPlannedTime(), todo.getStatus());
        StatusManager.changeTaskStatus(updatedTodoDTO, statusToChange);

        Todo updatedTodo = mapper.update(todo, updatedTodoDTO);

        try {
            todoRepository.save(updatedTodo);
        } catch (DateTimeParseException ex) {
            throw new LocalDateTimeException(Translator.toLocale("local.date.time.exception"));
        }

        return updatedTodoDTO;
    }

    @Override
    public Todo getTodoById(Long id) throws TaskNotFoundException {
        return todoRepository.findById(id).orElseThrow(() -> new TaskNotFoundException(
                String.format(Translator.toLocale("task.not.found"), id)));
    }

    @Override
    public TodoDTO getById(Authentication authentication, Long id) throws TaskNotFoundException {
        PersonDetails personDetails = (PersonDetails) authentication.getPrincipal();
        if (isAdmin(personDetails)) {
            return mapper.todoToTodoDTO(getTodoById(id));
        } else {
            return todoRepository.findByIdAndPersonName(id, personDetails.getUsername())
                    .map(mapper::todoToTodoDTO)
                    .orElseThrow(() -> new TaskNotFoundException(
                            String.format(Translator.toLocale("task.not.found"), id)));
        }
    }

    @Override
    public void deleteById(Long id) throws TaskNotFoundException {
        this.getTodoById(id);
        todoRepository.deleteById(id);
    }

    private PersonDetails getPersonDetailsFromContext() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return (PersonDetails) authentication.getPrincipal();
    }

    private static boolean isAdmin(PersonDetails personDetails) {
        return personDetails.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .anyMatch(role -> role.equals("ADMIN"));
    }
}