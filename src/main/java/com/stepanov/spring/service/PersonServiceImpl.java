package com.stepanov.spring.service;

import com.stepanov.spring.dto.NoIdPersonDTO;
import com.stepanov.spring.dto.PersonDTO;
import com.stepanov.spring.entity.Person;
import com.stepanov.spring.entity.Role;
import com.stepanov.spring.exception.DateTimeException;
import com.stepanov.spring.exception.PageNotFoundException;
import com.stepanov.spring.exception.PersonIsNotValidException;
import com.stepanov.spring.exception.PersonNotFoundException;
import com.stepanov.spring.mapper.PersonMapper;
import com.stepanov.spring.repository.PersonRepository;
import com.stepanov.spring.util.Translator;
import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.format.DateTimeParseException;
import java.util.List;
import java.util.Set;

@Service
public class PersonServiceImpl implements PersonService {
    private final PasswordEncoder passwordEncoder;
    private final PersonRepository personRepository;
    private final PersonMapper mapper;

    @Autowired
    public PersonServiceImpl(PasswordEncoder passwordEncoder, PersonRepository personRepository) {
        this.passwordEncoder = passwordEncoder;
        this.personRepository = personRepository;
        this.mapper = Mappers.getMapper(PersonMapper.class);
    }

    @Override
    public PersonDTO save(NoIdPersonDTO requestPerson) throws PersonIsNotValidException {
        PersonDTO personDTO = new PersonDTO
                (null, requestPerson.name(), passwordEncoder.encode(requestPerson.password()),
                        requestPerson.dateOfBirth(), requestPerson.ipn());

        if (isDuplicateIpn(personDTO)) {
            throw new PersonIsNotValidException(
                    String.format(Translator.toLocale("duplicate.person.ipn"), personDTO.ipn()));
        }

        Person person = mapper.personDtoToPerson(personDTO);

        Role userRole = new Role(2L, "USER");
        person.setRoles(Set.of(userRole));

        try {
            personRepository.save(person);
        } catch (DateTimeParseException ex) {
            throw new DateTimeException(Translator.toLocale("date.time.exception"));
        }
        return mapper.personToPersonDTO(person);
    }

    @Override
    public PersonDTO getById(Long id) throws PersonNotFoundException {
        return mapper.personToPersonDTO(getPersonById(id));
    }

    public Person getPersonById(Long id) throws PersonNotFoundException {
        return personRepository.findById(id).orElseThrow(() ->
                new PersonNotFoundException(String.format(Translator.toLocale("person.id.not.found"), id)));
    }

    @Override
    public List<PersonDTO> getAll(Pageable pageable) throws PageNotFoundException {
        return personRepository.findAll(pageable).stream().map(mapper::personToPersonDTO).toList();
    }

    @Override
    public PersonDTO update(Long id, NoIdPersonDTO requestPerson) throws PersonIsNotValidException, DateTimeException {
        PersonDTO personDTO = new PersonDTO
                (null, requestPerson.name(), passwordEncoder.encode(requestPerson.password()),
                        requestPerson.dateOfBirth(), requestPerson.ipn());

        if (isDuplicateIpn(personDTO)) {
            throw new PersonIsNotValidException(
                    String.format(Translator.toLocale("duplicate.person.ipn"), personDTO.ipn()));
        }
        Person person = this.getPersonById(id);
        Person updatedPerson = mapper.update(person, personDTO);
        updatedPerson.setId(id);
        try {
            personRepository.save(updatedPerson);
        } catch (DateTimeParseException ex) {
            throw new DateTimeException(Translator.toLocale("date.time.exception"));
        }
        return mapper.personToPersonDTO(updatedPerson);
    }

    @Override
    public void deleteById(Long id) throws PersonNotFoundException {
        this.getPersonById(id);
        personRepository.deleteById(id);
    }

    private boolean isDuplicateIpn(PersonDTO personDTO) {
        List<Person> all = (List<Person>) personRepository.findAll();
        return all.stream()
                .anyMatch(person -> person.getIpn().equals(personDTO.ipn()));
    }
}
