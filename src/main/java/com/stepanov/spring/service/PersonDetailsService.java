package com.stepanov.spring.service;

import com.stepanov.spring.entity.Person;
import com.stepanov.spring.entity.PersonDetails;
import com.stepanov.spring.exception.PersonNotFoundException;
import com.stepanov.spring.repository.PersonRepository;
import com.stepanov.spring.util.Translator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PersonDetailsService implements UserDetailsService {
    private final PersonRepository personRepository;

    @Autowired
    public PersonDetailsService(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws PersonNotFoundException {
        Optional<Person> person = personRepository.findByName(username);
        if (person.isEmpty()) {
            throw new PersonNotFoundException(String.format(Translator.toLocale("person.not.found"), username));
        }
        return new PersonDetails(person.get());
    }
}
