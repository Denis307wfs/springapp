package com.stepanov.spring.service;

import com.stepanov.spring.dto.AdminChangeTodoDTO;
import com.stepanov.spring.dto.CreateTodoDTO;
import com.stepanov.spring.dto.TodoDTO;
import com.stepanov.spring.entity.Todo;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;

import java.util.List;

public interface TodoService {
    TodoDTO save(CreateTodoDTO todoDTO);
    List<TodoDTO> getAll(Pageable pageable);
    List<TodoDTO> getAllForPerson(Pageable pageable);
    TodoDTO update(Long id, AdminChangeTodoDTO requestTask);
    Todo getTodoById(Long id);
    TodoDTO getById(Authentication authentication, Long id);
    void deleteById(Long id);
}
