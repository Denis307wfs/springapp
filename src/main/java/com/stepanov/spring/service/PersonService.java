package com.stepanov.spring.service;

import com.stepanov.spring.dto.NoIdPersonDTO;
import com.stepanov.spring.dto.PersonDTO;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface PersonService {
    PersonDTO save(NoIdPersonDTO personDTO);
    PersonDTO getById(Long id);
    List<PersonDTO> getAll(Pageable pageable);
    PersonDTO update(Long id, NoIdPersonDTO personDTO);
    void deleteById(Long id);
}
