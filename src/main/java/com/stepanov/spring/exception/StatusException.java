package com.stepanov.spring.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class StatusException extends RuntimeException {

    public StatusException(String message) {
        super(String.format(message));
    }
}
