package com.stepanov.spring.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class PersonIsNotValidException extends RuntimeException {
    public PersonIsNotValidException(String message) {
        super(message);
    }
}
