package com.stepanov.spring.repository;

import com.stepanov.spring.entity.Person;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PersonRepository extends CrudRepository<Person, Long>, PagingAndSortingRepository<Person, Long> {
    @NonNull
    @EntityGraph(attributePaths = "roles")
    Page<Person> findAll(@NonNull Pageable pageable);

    Optional<Person> findByName(String name);
}
