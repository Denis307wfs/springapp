package com.stepanov.spring.repository;

import com.stepanov.spring.entity.Todo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TodoRepository extends JpaRepository<Todo, Long> {
    @NonNull
    @EntityGraph(attributePaths = "person")
    Page<Todo> findAll(@NonNull Pageable pageable);

    List<Todo> findAllByPersonName(Pageable pageable, String personName);

    Optional<Todo> findByIdAndPersonName(Long id, String personName);
}
