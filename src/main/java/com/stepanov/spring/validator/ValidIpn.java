package com.stepanov.spring.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

import static java.lang.annotation.ElementType.*;

@Target({FIELD, METHOD, PARAMETER, ANNOTATION_TYPE, TYPE_USE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = IpnValidator.class)
@Documented
@Repeatable(ValidIpn.List.class)
public @interface ValidIpn {
    String message() default "{com.stepanov.validator.ValidIpn, Incorrect ipn}";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default { };

    @Target({FIELD, METHOD, PARAMETER, ANNOTATION_TYPE})
    @Retention(RetentionPolicy.RUNTIME)
    @Documented
    @interface List{
        ValidIpn[] value();
    }
}
