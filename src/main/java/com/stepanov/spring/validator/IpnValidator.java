package com.stepanov.spring.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class IpnValidator implements ConstraintValidator<ValidIpn, String> {
    private static final int[] WEIGHT = {-1, 5, 7, 9, 4, 6, 10, 5, 7};

    @Override
    public void initialize(ValidIpn constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    @Override
    public boolean isValid(String ipn, ConstraintValidatorContext constraintValidatorContext) {
        return checkLastControlDigit(constraintValidatorContext, ipn);
    }

    private void updateConstraintViolationMessage(ConstraintValidatorContext context, String message) {
        context.disableDefaultConstraintViolation();
        context.buildConstraintViolationWithTemplate(message).addConstraintViolation();
    }

    private boolean checkLastControlDigit(ConstraintValidatorContext constraintValidatorContext, String ipn) {
        int lastDigit = 0;
        int sum = 0;
        try {
            lastDigit = ipn.charAt(ipn.length() - 1) - '0';
            for (int i = 0; i < ipn.length() - 1; i++) {
                sum += (ipn.charAt(i) - '0') * WEIGHT[i];
            }
        } catch (ArrayIndexOutOfBoundsException | StringIndexOutOfBoundsException e) {
            updateConstraintViolationMessage(constraintValidatorContext,
                    "incorrect.ipn");
        }
        updateConstraintViolationMessage(constraintValidatorContext, "incorrect.ipn");

        return sum % 11 % 10 == lastDigit;
    }
}
