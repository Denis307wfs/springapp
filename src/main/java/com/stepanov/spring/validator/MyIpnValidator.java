package com.stepanov.spring.validator;

import com.stepanov.spring.entity.Person;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import org.hibernate.validator.messageinterpolation.ParameterMessageInterpolator;
import org.springframework.stereotype.Component;

import java.util.Set;

@Component
public class MyIpnValidator {
    private final Validator validator;
    private Set<ConstraintViolation<Person>> violations;

    public MyIpnValidator() {
        ValidatorFactory factory = Validation.byDefaultProvider()
                .configure()
                .messageInterpolator(new ParameterMessageInterpolator())
                .buildValidatorFactory();
        validator = factory.getValidator();
    }

    public boolean isValid(Person person) {
        violations = validator.validate(person);
        return violations.isEmpty();
    }

    public Set<ConstraintViolation<Person>> getViolations() {
        return violations;
    }
}
