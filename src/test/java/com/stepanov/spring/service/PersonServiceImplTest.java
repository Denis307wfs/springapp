package com.stepanov.spring.service;

import com.stepanov.spring.dto.NoIdPersonDTO;
import com.stepanov.spring.dto.PersonDTO;
import com.stepanov.spring.entity.Person;
import com.stepanov.spring.exception.DateTimeException;
import com.stepanov.spring.exception.PersonIsNotValidException;
import com.stepanov.spring.repository.PersonRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.time.LocalDate;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class PersonServiceImplTest {
    @InjectMocks
    private PersonServiceImpl personService;
    @Mock
    private PasswordEncoder passwordEncoder;
    @Mock
    private PersonRepository personRepository;

    @Test
    void saveValidPersonDtoTest() throws PersonIsNotValidException {
        NoIdPersonDTO personDTO = new NoIdPersonDTO
                ("John", "123", LocalDate.of(1980, 10, 12), "1234567899");

        Person person = new Person
                (null, "John", "123", LocalDate.of(1980, 10, 12), "1234567899");

        when(passwordEncoder.encode(anyString())).thenReturn("123");
        when(personRepository.save(any(Person.class))).thenReturn(person);

        PersonDTO savedPersonDTO = personService.save(personDTO);

        verify(personRepository, times(1)).save(person);

        assertEquals(personDTO.name(), savedPersonDTO.name());
        assertEquals(personDTO.password(), savedPersonDTO.password());
        assertEquals(personDTO.dateOfBirth(), savedPersonDTO.dateOfBirth());
        assertEquals(personDTO.ipn(), savedPersonDTO.ipn());
    }

    @Test
    void updateValidPersonDtoTest() throws PersonIsNotValidException, DateTimeException {
        NoIdPersonDTO personDTO = new NoIdPersonDTO
                ("John", "123", LocalDate.of(1980, 10, 12), "1234567899");

        Person person = new Person
                (null, "Den", "123", LocalDate.of(1980, 10, 12), "1111111111");

        when(passwordEncoder.encode(anyString())).thenReturn("123");
        when(personRepository.findById(anyLong())).thenReturn(Optional.of(person));
        when(personRepository.save(any(Person.class))).thenReturn(person);

        PersonDTO updatedPersonDTO = personService.update(2L, personDTO);

        assertEquals(personDTO.name(), updatedPersonDTO.name());
        assertEquals(personDTO.password(), updatedPersonDTO.password());
        assertEquals(personDTO.dateOfBirth(), updatedPersonDTO.dateOfBirth());
        assertEquals(personDTO.ipn(), updatedPersonDTO.ipn());
    }
}