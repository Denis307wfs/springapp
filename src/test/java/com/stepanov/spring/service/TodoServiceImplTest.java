package com.stepanov.spring.service;

import com.stepanov.spring.dto.AdminChangeTodoDTO;
import com.stepanov.spring.dto.TodoDTO;
import com.stepanov.spring.entity.Person;
import com.stepanov.spring.entity.PersonDetails;
import com.stepanov.spring.entity.Role;
import com.stepanov.spring.entity.Todo;
import com.stepanov.spring.enums.Status;
import com.stepanov.spring.exception.LocalDateTimeException;
import com.stepanov.spring.exception.TaskNotFoundException;
import com.stepanov.spring.repository.TodoRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.Authentication;

import java.time.LocalDateTime;
import java.time.Month;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;


@ExtendWith(MockitoExtension.class)
class TodoServiceImplTest {
    @InjectMocks
    private TodoServiceImpl todoService;
    @Mock
    private TodoRepository todoRepository;
    @Mock
    private Authentication authentication;

    @Test
    void updateTaskTest() throws LocalDateTimeException {
        AdminChangeTodoDTO todoDtoToChange = new AdminChangeTodoDTO(
                "Test", "2023-12-12 09:09", "SIGNED");

        Todo todo = new Todo();
        todo.setId(3L);
        todo.setName("Old name");
        todo.setPlannedTime(LocalDateTime.now());
        todo.setStatus(Status.POSTPONED);

        when(todoRepository.findById(anyLong())).thenReturn(Optional.of(todo));
        when(todoRepository.save(any(Todo.class))).thenReturn(todo);

        TodoDTO updatedTodo = todoService.update(3L, todoDtoToChange);

        assertEquals(todoDtoToChange.getName(), updatedTodo.getName());
        assertEquals(todoDtoToChange.getPlannedTime(), updatedTodo.getPlannedTime());
        assertEquals(todoDtoToChange.getStatus(), String.valueOf(updatedTodo.getStatus()));
    }

    @Test
    void getTaskByIdWhenUserInvokeTest() throws TaskNotFoundException {
        TodoDTO todoDTO = new TodoDTO(4L, "User Test", "2023-12-12 09:09", Status.NOTIFIED);

        Todo todo = new Todo();
        todo.setId(4L);
        todo.setName("User Test");
        todo.setPlannedTime(LocalDateTime.of(2023, Month.DECEMBER, 12, 9, 9));
        todo.setStatus(Status.NOTIFIED);

        Person userPerson = new Person();
        userPerson.setName("Ben");
        userPerson.setRoles(Set.of(new Role(2L, "USER")));
        PersonDetails personDetails = new PersonDetails(userPerson);

        when(authentication.getPrincipal()).thenReturn(personDetails);
        when(todoRepository.findByIdAndPersonName(anyLong(), anyString())).thenReturn(Optional.of(todo));

        TodoDTO returnedTodoDTO = todoService.getById(authentication, 4L);

        assertEquals(todoDTO.getId(), returnedTodoDTO.getId());
        assertEquals(todoDTO.getName(), returnedTodoDTO.getName());
        assertEquals(todoDTO.getPlannedTime(), returnedTodoDTO.getPlannedTime());
        assertEquals(todoDTO.getStatus(), returnedTodoDTO.getStatus());
    }

    @Test
    void getTaskByIdWhenAdminInvokeTest() throws TaskNotFoundException {
        TodoDTO todoDTO = new TodoDTO(4L, "Admin Test", "2023-12-12 09:09", Status.NOTIFIED);

        Todo todo = new Todo();
        todo.setId(4L);
        todo.setName("Admin Test");
        todo.setPlannedTime(LocalDateTime.of(2023, Month.DECEMBER, 12, 9, 9));
        todo.setStatus(Status.NOTIFIED);

        Person userPerson = new Person();
        userPerson.setName("Ben");
        userPerson.setRoles(Set.of(new Role(1L, "ADMIN"), new Role(2L, "USER")));
        PersonDetails personDetails = new PersonDetails(userPerson);

        when(authentication.getPrincipal()).thenReturn(personDetails);
        when(todoRepository.findById(anyLong())).thenReturn(Optional.of(todo));

        TodoDTO returnedTodoDTO = todoService.getById(authentication, 4L);

        assertEquals(todoDTO.getId(), returnedTodoDTO.getId());
        assertEquals(todoDTO.getName(), returnedTodoDTO.getName());
        assertEquals(todoDTO.getPlannedTime(), returnedTodoDTO.getPlannedTime());
        assertEquals(todoDTO.getStatus(), returnedTodoDTO.getStatus());
    }
}