package com.stepanov.spring.mapper;

import com.stepanov.spring.dto.CreateTodoDTO;
import com.stepanov.spring.entity.Todo;
import com.stepanov.spring.enums.Status;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CreateTodoMapperTest {
    private final TodoMapper mapper = Mappers.getMapper(TodoMapper.class);

    @Test
    void mapCreateTodoDtoTest() {
        CreateTodoDTO dto = new CreateTodoDTO("Name", "2000-01-01 12:32");

        Todo todo = mapper.createTodoDTOtoTodo(dto);

        assertEquals(dto.getName(), todo.getName());
        assertEquals(Status.PLANNED, todo.getStatus());
    }
}