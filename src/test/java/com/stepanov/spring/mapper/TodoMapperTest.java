package com.stepanov.spring.mapper;

import com.stepanov.spring.dto.TodoDTO;
import com.stepanov.spring.enums.Status;
import com.stepanov.spring.entity.Todo;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static org.junit.jupiter.api.Assertions.assertEquals;

class TodoMapperTest {
    private final TodoMapper mapper = Mappers.getMapper(TodoMapper.class);
    private static final String DATE_FORMAT = "yyyy-MM-dd HH:mm";

    @Test
    void mapTodoToTodoDTOTest() {
        Todo todo = new Todo();
        todo.setId(2L);
        todo.setName("Read book");
        LocalDateTime dateTime = LocalDateTime.of(1991, 8, 4, 12, 22);
        todo.setPlannedTime(dateTime);
        todo.setStatus(Status.PLANNED);

        TodoDTO todoDTO = mapper.todoToTodoDTO(todo);

        String dateTimeFormatted = dateTime.format(DateTimeFormatter.ofPattern(DATE_FORMAT));

        assertEquals(todo.getName(), todoDTO.getName());
        assertEquals(dateTimeFormatted, todoDTO.getPlannedTime());
        assertEquals(todo.getStatus(), todoDTO.getStatus());
    }

    @Test
    void mapTodoDTOtoTodoTest() {
        TodoDTO todoDTO = new TodoDTO(
                2L, "Read book", "1991-08-04 12:22", Status.NOTIFIED);

        Todo todo = mapper.todoDTOtoTodo(todoDTO);

        LocalDateTime localDateTime = LocalDateTime.parse(todoDTO.getPlannedTime(), DateTimeFormatter.ofPattern(DATE_FORMAT));

        assertEquals(todoDTO.getName(), todo.getName());
        assertEquals(localDateTime, todo.getPlannedTime());
        assertEquals(todoDTO.getStatus(), todo.getStatus());
    }
}