package com.stepanov.spring.controller;

import com.stepanov.spring.dto.NoIdPersonDTO;
import com.stepanov.spring.dto.PersonDTO;
import com.stepanov.spring.service.PersonService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@WebMvcTest(PersonController.class)
@WithMockUser(username = "Mark", password = "123", roles = "ADMIN")
class PersonControllerTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private PersonService personService;

    @Test
    void givenPersonsWhenGetPersonsThenReturnJsonArray() throws Exception {
        PersonDTO personDTO = new PersonDTO
                (1L, "John", "123", LocalDate.of(1980, 10, 12), "1234567899");
        PersonDTO personDTO2 = new PersonDTO
                (2L, "Mary", "123", LocalDate.of(1988, 3, 8), "3652512345");
        List<PersonDTO> persons = List.of(personDTO, personDTO2);

        given(personService.getAll(any(Pageable.class))).willReturn(persons);

        mvc.perform(get("/persons/")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].name", is(personDTO.name())))
                .andExpect(jsonPath("$[0].ipn", is(personDTO.ipn())))
                .andExpect(jsonPath("$[1].name", is(personDTO2.name())))
                .andExpect(jsonPath("$[1].ipn", is(personDTO2.ipn())));
    }

    @Test
    void givenPersonWhenGetPersonIdThenReturnJson() throws Exception {
        long personIdToGet = 2L;
        PersonDTO personDTO = new PersonDTO
                (personIdToGet, "John", "123", LocalDate.of(1980, 10, 12), "1234567899");

        given(personService.getById(personIdToGet)).willReturn(personDTO);

        mvc.perform(get("/persons/" + personIdToGet)
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(personDTO.id().intValue())))
                .andExpect(jsonPath("$.name", is(personDTO.name())))
                .andExpect(jsonPath("$.ipn", is(personDTO.ipn())));
    }

    @Test
    void updatePersonWhenPutPersonId() throws Exception {
        long personIdToPut = 2L;
        PersonDTO personDTO = new PersonDTO
                (personIdToPut, "John", "123", LocalDate.of(1980, 10, 12), "1234567899");

        String personJson = """
                {
                    "name": "John",
                    "password": "111",
                    "dateOfBirth": "1980-10-12",
                    "ipn": "1234567899"
                }
                """;

        given(personService.update(anyLong(), any(NoIdPersonDTO.class))).willReturn(personDTO);

        mvc.perform(put("/persons/" + personIdToPut)
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(personJson))
                .andExpect(status().isOk());
    }

    @Test
    void createPersonWhenPostPerson() throws Exception {
        PersonDTO personDTO = new PersonDTO
                (3L, "John", "123", LocalDate.of(1980, 10, 12), "1234567899");

        String personJson = """
                {
                    "name": "John",
                    "password": "111",
                    "dateOfBirth": "1980-10-12",
                    "ipn": "1234567899"
                }
                """;

        given(personService.save(any(NoIdPersonDTO.class))).willReturn(personDTO);

        mvc.perform(post("/persons")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(personJson))
                .andExpect(status().isCreated());
    }

    @Test
    void deletePersonWhenDeletePersonId() throws Exception {
        long personIdToDelete = 2L;

        mvc.perform(delete("/persons/" + personIdToDelete)
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }
}