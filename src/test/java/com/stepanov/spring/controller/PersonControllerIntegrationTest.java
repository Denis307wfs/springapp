package com.stepanov.spring.controller;


import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.stepanov.spring.dto.NoIdPersonDTO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.net.URI;
import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class PersonControllerIntegrationTest {

    @Autowired
    private TestRestTemplate restTemplate;


    @Test
    void shouldReturnPersonWhenDataIsSaved() {
        ResponseEntity<String> response = restTemplate
                .withBasicAuth("Mark", "123")
                .getForEntity("/persons/3", String.class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);

        DocumentContext documentContext = JsonPath.parse(response.getBody());
        Number id = documentContext.read("$.id");
        String name = documentContext.read("$.name");
        assertThat(id).isEqualTo(3);
        assertThat(name).isEqualTo("Kate");
    }

    @Test
    void shouldNotReturnAPersonWithAnUnknownId() {
        ResponseEntity<String> response = restTemplate
                .withBasicAuth("Mark", "123")
                .getForEntity("/persons/66", String.class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    @Test
    void shouldCreateANewPerson() {
        NoIdPersonDTO person = new NoIdPersonDTO( "Jack", "123", LocalDate.of(2000, 1, 1), "1234567899");
        ResponseEntity<Void> createResponse = restTemplate
                .withBasicAuth("Mark", "123")
                .postForEntity("/persons", person, Void.class);
        assertThat(createResponse.getStatusCode()).isEqualTo(HttpStatus.CREATED);

        URI locationOfNewPerson = createResponse.getHeaders().getLocation();
        ResponseEntity<String> getResponse = restTemplate
                .withBasicAuth("Mark", "123")
                .getForEntity(locationOfNewPerson, String.class);
        assertThat(getResponse.getStatusCode()).isEqualTo(HttpStatus.OK);

        DocumentContext documentContext = JsonPath.parse(getResponse.getBody());
        Number id = documentContext.read("$.id");
        String name = documentContext.read("$.name");

        assertThat(id).isNotNull();
        assertThat(name).isEqualTo("Jack");
    }
}