package com.stepanov.spring;

import com.stepanov.spring.entity.Person;
import com.stepanov.spring.entity.Role;
import com.stepanov.spring.entity.Todo;
import com.stepanov.spring.enums.Status;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.json.JacksonTester;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

@org.springframework.boot.test.autoconfigure.json.JsonTest
class JsonTest {

    @Autowired
    private JacksonTester<Person> json;

    @Test
    void personSerializationTest() throws IOException {
        Person person = new Person(4L, "Nick", LocalDate.of(2000, 1, 1), "3652512345");
        person.setRoles(Set.of(new Role(1, "ADMIN")));

        Todo todo = new Todo();
        todo.setId(2L);
        todo.setName("Work");
        todo.setPlannedTime(LocalDateTime.of(2023, 10, 19, 10, 20));
        todo.setStatus(Status.PLANNED);

        person.setTasks(List.of(todo));

        assertThat(json.write(person)).isStrictlyEqualToJson("expected.json");
        assertThat(json.write(person)).hasJsonPathNumberValue("@.id");
        assertThat(json.write(person)).extractingJsonPathNumberValue("@.id").isEqualTo(4);
        assertThat(json.write(person)).hasJsonPath("@.name");
        assertThat(json.write(person)).extractingJsonPathStringValue("@.name").isEqualTo("Nick");
        assertThat(json.write(person)).hasJsonPath("@.ipn");
        assertThat(json.write(person)).extractingJsonPathStringValue("@.ipn").isEqualTo("3652512345");
        assertThat(json.write(person)).hasJsonPath("@.tasks");
    }

    @Test
    void personDeserializationTest() throws IOException {
        Person person = new Person(3L, "John", LocalDate.of(2000, 1, 1), "3652512345");

        Todo todo = new Todo();
        todo.setId(2L);
        todo.setName("Work");
        todo.setPlannedTime(LocalDateTime.of(2023, 10, 19, 10, 20));
        todo.setStatus(Status.PLANNED);
        person.setTasks(List.of(todo));

        String expected = """
                {
                "id": 3,
                "name": "John",
                "dateOfBirth": "2000-01-01",
                "ipn": "3652512345",
                "tasks": [{
                	"id": 2,
                	"name": "Work",
                	"plannedTime": "2023-10-19T10:20:00",
                	"status": "PLANNED"}],
                "role": null}
                """;

        assertThat(json.parse(expected).getObject()).isEqualTo(person);
        assertThat(json.parseObject(expected).getId()).isEqualTo(3);
        assertThat(json.parseObject(expected).getName()).isEqualTo("John");
        assertThat(json.parseObject(expected).getDateOfBirth()).isEqualTo(LocalDate.of(2000, 1, 1));
        assertThat(json.parseObject(expected).getIpn()).isEqualTo("3652512345");
        assertThat(json.parseObject(expected).getTasks()).isEqualTo(List.of(todo));
    }
}
