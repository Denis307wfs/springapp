package com.stepanov.spring.entity;

import com.stepanov.spring.dto.TodoDTO;
import com.stepanov.spring.enums.Status;
import com.stepanov.spring.exception.StatusException;
import com.stepanov.spring.util.StatusManager;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StatusTest {
    private final Status PLANNED = Status.PLANNED;
    private final Status WORK_IN_PROGRESS = Status.WORK_IN_PROGRESS;
    private final Status POSTPONED = Status.POSTPONED;
    private final Status NOTIFIED = Status.NOTIFIED;
    private final Status SIGNED = Status.SIGNED;
    private final Status DONE = Status.DONE;
    private final Status CANCELLED = Status.CANCELLED;

    @Test
    void notAllowedStatusToChangeFromPlannedTest() {
        TodoDTO todoDTO = new TodoDTO(2L, "Work", "2023-10-10", Status.PLANNED);

        assertThrows(StatusException.class, () -> StatusManager.changeTaskStatus(todoDTO, NOTIFIED));
        assertThrows(StatusException.class, () -> StatusManager.changeTaskStatus(todoDTO, SIGNED));
        assertThrows(StatusException.class, () -> StatusManager.changeTaskStatus(todoDTO, DONE));
    }

    @Test
    void allowedStatusToChangeFromPlannedTest1() {
        TodoDTO todoDTO = new TodoDTO(2L, "Work", "2023-10-10", Status.PLANNED);

        assertTrue(StatusManager.changeTaskStatus(todoDTO, PLANNED));
        assertTrue(StatusManager.changeTaskStatus(todoDTO, WORK_IN_PROGRESS));
    }

    @Test
    void allowedStatusToChangeFromPlannedTest2() {
        TodoDTO todoDTO = new TodoDTO(2L, "Work", "2023-10-10", Status.PLANNED);

        assertTrue(StatusManager.changeTaskStatus(todoDTO, POSTPONED));
    }

    @Test
    void allowedStatusToChangeFromPlannedTest3() {
        TodoDTO todoDTO = new TodoDTO(2L, "Work", "2023-10-10", Status.PLANNED);

        assertTrue(StatusManager.changeTaskStatus(todoDTO, CANCELLED));
    }

    @Test
    void notAllowedStatusToChangeFromWorkInProgressTest() {
        TodoDTO todoDTO = new TodoDTO(2L, "Work", "2023-10-10", Status.WORK_IN_PROGRESS);

        assertThrows(StatusException.class, () -> StatusManager.changeTaskStatus(todoDTO, POSTPONED));
        assertThrows(StatusException.class, () -> StatusManager.changeTaskStatus(todoDTO, PLANNED));
        assertThrows(StatusException.class, () -> StatusManager.changeTaskStatus(todoDTO, DONE));
    }

    @Test
    void allowedStatusToChangeFromWorkInProgressTest1() {
        TodoDTO todoDTO = new TodoDTO(2L, "Work", "2023-10-10", Status.WORK_IN_PROGRESS);

        assertTrue(StatusManager.changeTaskStatus(todoDTO, WORK_IN_PROGRESS));
        assertTrue(StatusManager.changeTaskStatus(todoDTO, NOTIFIED));
    }

    @Test
    void allowedStatusToChangeFromWorkInProgressTest2() {
        TodoDTO todoDTO = new TodoDTO(2L, "Work", "2023-10-10", Status.WORK_IN_PROGRESS);

        assertTrue(StatusManager.changeTaskStatus(todoDTO, SIGNED));
    }

    @Test
    void allowedStatusToChangeFromWorkInProgressTest3() {
        TodoDTO todoDTO = new TodoDTO(2L, "Work", "2023-10-10", Status.WORK_IN_PROGRESS);

        assertTrue(StatusManager.changeTaskStatus(todoDTO, CANCELLED));
    }

    @Test
    void notAllowedStatusToChangeFromPostponedTest() {
        TodoDTO todoDTO = new TodoDTO(2L, "Work", "2023-10-10", Status.POSTPONED);

        assertThrows(StatusException.class, () -> StatusManager.changeTaskStatus(todoDTO, WORK_IN_PROGRESS));
        assertThrows(StatusException.class, () -> StatusManager.changeTaskStatus(todoDTO, PLANNED));
        assertThrows(StatusException.class, () -> StatusManager.changeTaskStatus(todoDTO, DONE));
    }

    @Test
    void allowedStatusToChangeFromPostponedTest1() {
        TodoDTO todoDTO = new TodoDTO(2L, "Work", "2023-10-10", Status.POSTPONED);

        assertTrue(StatusManager.changeTaskStatus(todoDTO, POSTPONED));
        assertTrue(StatusManager.changeTaskStatus(todoDTO, NOTIFIED));
    }

    @Test
    void allowedStatusToChangeFromPostponedTest2() {
        TodoDTO todoDTO = new TodoDTO(2L, "Work", "2023-10-10", Status.POSTPONED);

        assertTrue(StatusManager.changeTaskStatus(todoDTO, SIGNED));
    }

    @Test
    void allowedStatusToChangeFromPostponedTest3() {
        TodoDTO todoDTO = new TodoDTO(2L, "Work", "2023-10-10", Status.POSTPONED);

        assertTrue(StatusManager.changeTaskStatus(todoDTO, CANCELLED));
    }

    @Test
    void notAllowedStatusToChangeFromNotifiedTest() {
        TodoDTO todoDTO = new TodoDTO(2L, "Work", "2023-10-10", Status.NOTIFIED);

        assertThrows(StatusException.class, () -> StatusManager.changeTaskStatus(todoDTO, PLANNED));
        assertThrows(StatusException.class, () -> StatusManager.changeTaskStatus(todoDTO, WORK_IN_PROGRESS));
        assertThrows(StatusException.class, () -> StatusManager.changeTaskStatus(todoDTO, POSTPONED));
        assertThrows(StatusException.class, () -> StatusManager.changeTaskStatus(todoDTO, SIGNED));
    }

    @Test
    void allowedStatusToChangeFromNotifiedTest1() {
        TodoDTO todoDTO = new TodoDTO(2L, "Work", "2023-10-10", Status.NOTIFIED);

        assertTrue(StatusManager.changeTaskStatus(todoDTO, NOTIFIED));
        assertTrue(StatusManager.changeTaskStatus(todoDTO, DONE));
    }

    @Test
    void allowedStatusToChangeFromNotifiedTest2() {
        TodoDTO todoDTO = new TodoDTO(2L, "Work", "2023-10-10", Status.NOTIFIED);

        assertTrue(StatusManager.changeTaskStatus(todoDTO, CANCELLED));
    }

    @Test
    void notAllowedStatusToChangeFromSignedTest() {
        TodoDTO todoDTO = new TodoDTO(2L, "Work", "2023-10-10", Status.SIGNED);

        assertThrows(StatusException.class, () -> StatusManager.changeTaskStatus(todoDTO, PLANNED));
        assertThrows(StatusException.class, () -> StatusManager.changeTaskStatus(todoDTO, WORK_IN_PROGRESS));
        assertThrows(StatusException.class, () -> StatusManager.changeTaskStatus(todoDTO, POSTPONED));
        assertThrows(StatusException.class, () -> StatusManager.changeTaskStatus(todoDTO, NOTIFIED));
    }

    @Test
    void allowedStatusToChangeFromSignedTest1() {
        TodoDTO todoDTO = new TodoDTO(2L, "Work", "2023-10-10", Status.SIGNED);

        assertTrue(StatusManager.changeTaskStatus(todoDTO, SIGNED));
        assertTrue(StatusManager.changeTaskStatus(todoDTO, DONE));
    }

    @Test
    void allowedStatusToChangeFromSignedTest2() {
        TodoDTO todoDTO = new TodoDTO(2L, "Work", "2023-10-10", Status.SIGNED);

        assertTrue(StatusManager.changeTaskStatus(todoDTO, CANCELLED));
    }

    @Test
    void notAllowedStatusToChangeFromDoneTest() {
        TodoDTO todoDTO = new TodoDTO(2L, "Work", "2023-10-10", Status.DONE);

        assertTrue(StatusManager.changeTaskStatus(todoDTO, DONE));

        assertThrows(StatusException.class, () -> StatusManager.changeTaskStatus(todoDTO, PLANNED));
        assertThrows(StatusException.class, () -> StatusManager.changeTaskStatus(todoDTO, WORK_IN_PROGRESS));
        assertThrows(StatusException.class, () -> StatusManager.changeTaskStatus(todoDTO, POSTPONED));
        assertThrows(StatusException.class, () -> StatusManager.changeTaskStatus(todoDTO, NOTIFIED));
        assertThrows(StatusException.class, () -> StatusManager.changeTaskStatus(todoDTO, SIGNED));
        assertThrows(StatusException.class, () -> StatusManager.changeTaskStatus(todoDTO, CANCELLED));
    }

    @Test
    void notAllowedStatusToChangeFromCanceledTest() {
        TodoDTO todoDTO = new TodoDTO(2L, "Work", "2023-10-10", Status.CANCELLED);

        assertTrue(StatusManager.changeTaskStatus(todoDTO, CANCELLED));

        assertThrows(StatusException.class, () -> StatusManager.changeTaskStatus(todoDTO, PLANNED));
        assertThrows(StatusException.class, () -> StatusManager.changeTaskStatus(todoDTO, WORK_IN_PROGRESS));
        assertThrows(StatusException.class, () -> StatusManager.changeTaskStatus(todoDTO, POSTPONED));
        assertThrows(StatusException.class, () -> StatusManager.changeTaskStatus(todoDTO, NOTIFIED));
        assertThrows(StatusException.class, () -> StatusManager.changeTaskStatus(todoDTO, SIGNED));
        assertThrows(StatusException.class, () -> StatusManager.changeTaskStatus(todoDTO, DONE));
    }

    @Test
    void allowedStatusToChangeFromPlannedTest() {
        TodoDTO todoDTO = new TodoDTO(2L, "Work", "2023-10-10", Status.PLANNED);

        boolean isAbleToWorkInProgress = isAbleToChangeStatus(todoDTO, WORK_IN_PROGRESS);
        boolean isAbleToPostponed = isAbleToChangeStatus(todoDTO, POSTPONED);
        boolean isAbleToCanceled = isAbleToChangeStatus(todoDTO, CANCELLED);

        boolean isAbleToNotified = isAbleToChangeStatus(todoDTO, NOTIFIED);
        boolean isAbleToSigned = isAbleToChangeStatus(todoDTO, SIGNED);
        boolean isAbleToDone = isAbleToChangeStatus(todoDTO, DONE);

        assertTrue(isAbleToWorkInProgress);
        assertTrue(isAbleToPostponed);
        assertTrue(isAbleToCanceled);

        assertFalse(isAbleToNotified);
        assertFalse(isAbleToSigned);
        assertFalse(isAbleToDone);

        todoDTO.setStatus(WORK_IN_PROGRESS);

        assertEquals(Status.WORK_IN_PROGRESS, todoDTO.getStatus());
    }

    private boolean isAbleToChangeStatus(TodoDTO todoDTO, Status statusToChange) {
        return todoDTO.getStatus().nextStatus().contains(statusToChange);
    }
}